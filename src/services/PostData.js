export function PostData(type=false, userData, token) {
    let BaseUrl = 'http://localhost:5000';

    return new Promise((resolve, reject) => {
        fetch(BaseUrl + type, {
            headers: { 
                'Content-Type': 'application/json',
                'x-token': token 
            },
            method: 'POST',
            dataType: 'JSON',
            body: JSON.stringify(userData)
        })
        .then((response) => response.json())
        .then((responseJson) => {
            resolve(responseJson);
        })
        .catch((error) => {
            reject(error);
        });
    });
}