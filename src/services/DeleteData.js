export function DeleteData(type=false, token) {
    let BaseUrl = 'http://localhost:5000';

    return new Promise((resolve, reject) => {
        fetch(BaseUrl + type, {
            headers: { 
                'Content-Type': 'application/json',
                'x-token': token 
            },
            method: 'DELETE',
            dataType: 'JSON'
        })
        .then((response) => response.json())
        .then((responseJson) => {
            resolve(responseJson);
        })
        .catch((error) => {
            reject(error);
        });
    });
}