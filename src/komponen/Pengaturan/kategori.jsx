import React, { Component } from 'react';
import Navnya from '../Menu/Navnya';
import Topnav from '../Menu/Topnav';
import '../Menu/Nav.css';
import '../style.css';
import { GetData } from '../../services/GetData';
import { PostData } from '../../services/PostData';
import { DeleteData } from '../../services/DeleteData';
import { PutData } from '../../services/PutData';
import { Redirect } from 'react-router-dom';
import { MDBDataTable } from 'mdbreact';

class Kategori extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            kategoris: '',
            id: '',
            tableRows: [],
            redirect: false
        }
        this.simpan = this.simpan.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    /* load saat membuka file */
    componentDidMount() {
        GetData('/categories/all', sessionStorage.getItem("token")).then((result) => {
            let responseJson = result;
            if (responseJson.state) {
                this.setState({ kategoris: responseJson.categories })
                this.setState({ tableRows: this.assemblePosts() })
            } else {
                alert(responseJson.msg);
                sessionStorage.setItem("token", '');
                sessionStorage.clear();
                this.setState({ redirect: true });
            }
        });
    }
    /* end load saat membuka file */

    /* menyimpan ke state apapun yang di ketik */
    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }
    /* end simpan ke state */

    /* looping isi setiap row untuk datatable */
    assemblePosts = () => {
        let kat = this.state.kategoris.map((kgr) => {
            return (

                {

                    id: kgr.id,

                    name: kgr.name,

                    aksi: <div><button type="button" className="btn btn-warning btn-sm" onClick={() => this.ubah(kgr.id)} data-toggle="modal" data-target="#Modaledit" >
                        <i className="fa fa-edit"></i> Ubah</button>
                        <button type="button" className="btn btn-danger btn-sm" onClick={() => this.hapus(kgr.id)} value={kgr.id} >
                            <i className="fa fa-eraser"></i> Hapus</button></div>


                }

            )

        });
        return kat;
    }
    /* end looping */

    /* start CRUD */
    simpan() {
        if (this.state.name) {
            PostData('/categories/add', this.state, sessionStorage.getItem("token")).then((result) => {
                let responseJson = result;
                alert(responseJson.msg);
                window.location.reload();
            });
        } else {
            alert('Data harus diisi');
        }
    }
    hapus = obj => {
        const x = window.confirm("Data akan dihapus?");
        if (x) {
            DeleteData('/categories/' + obj + '/delete', sessionStorage.getItem("token")).then((result) => {
                let responseJson = result;
                alert(responseJson.msg);
                window.location.reload();
            });
        } else {
            return false;
        }
    }
    ubah = obj => {
        GetData('/categories/' + obj + '/edit', sessionStorage.getItem("token")).then((result) => {
            let responseJson = result;
            this.setState({ name: responseJson.categories.name });
            this.setState({ id: responseJson.categories.id });
        });
    }
    update = obj => {
        if (this.state.name) {
            PutData('/categories/' + obj + '/update', this.state, sessionStorage.getItem("token")).then((result) => {
                let responseJson = result;
                alert(responseJson.msg);
                window.location.reload();
            });
        } else {
            alert('Data harus diisi');
        }
    }
    /* end CRUD */

    render() {
        /* route untuk ke halaman login */
        if (this.state.redirect) {
            return <Redirect to={"/login"} />
        }
        /* end route ke login */

        /* datatable */
        const data = {
            columns: [
                {
                    label: 'No',
                    field: 'id'
                },
                {
                    label: 'Kategori',
                    field: 'name'
                },
                {
                    label: 'Aksi',
                    field: 'aksi'
                }
            ],
            rows: this.state.tableRows
        };
        /* end datatable */

        return (
            <div>

                {/* halaman */}
                <Topnav />
                <div className="row" id="body-row">
                    <Navnya />
                    <div className="col content-wrapper">
                        <div className="right_col">
                            <h3><i className="fa fa-cogs"></i> Kategori</h3>
                            <div className="card">
                                <div className="card-body">
                                    <h5 className="card-title">Daftar Kategori<button type="button" className="float-right btn btn-primary btn-sm" data-toggle="modal" data-target="#Modalkategori">
                                        <i className="fa fa-plus"></i> Tambah</button></h5>
                                    <div className="table-responsive">
                                        <MDBDataTable striped bordered hover data={data} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* end halaman */}

                {/* modal tambah data */}
                <div className="modal fade" id="Modalkategori" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Tambah Kategori</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="form-group row">
                                    <label className="col-sm-2 col-form-label">Kategori</label>
                                    <div className="col-sm-10">
                                        <input type="text" className="form-control" name="name" placeholder="Kategori" onKeyPress={(event) => { if (event.key === "Enter") { this.simpan() } }} onChange={this.onChange} autoFocus></input>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="submit" name="submit" className="btn btn-primary" onClick={this.simpan}>Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* end modal tambah data */}

                {/* modal edit data */}
                <div className="modal fade" id="Modaledit" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Ubah Kategori</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="form-group row">
                                    <label className="col-sm-2 col-form-label">Kategori</label>
                                    <div className="col-sm-10">
                                        <input type="text" className="form-control" name="name" placeholder="Kategori" onChange={this.onChange} value={this.state.name} autoFocus></input>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="submit" name="submit" className="btn btn-primary" onClick={() => this.update(this.state.id)}>Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* end modal edit data */}

            </div>
        );
    }
}

export default Kategori;