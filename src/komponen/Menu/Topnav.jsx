import React, { Component } from 'react';
import './Nav.css';
import { Redirect, Link } from 'react-router-dom';
class Topnav extends Component {
    constructor(props) {
        super(props);
        this.state = {
            redirect: false
        }
        this.logout = this.logout.bind(this);
    }
    logout() {
        const x = window.confirm("Yakin keluar?");
        
        if (x) {
            sessionStorage.setItem("token", '');
            sessionStorage.clear();
            this.setState({ redirect: true });
            return true;
        } else {
            return false;
        }
    }

    render() {
        if (this.state.redirect) {
            return <Redirect to={"/login"} />
        }
        if (this.state.home) {
            return <Redirect to={"/"} />
        }
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                    <Link className="navbar-brand" to="/" onClick={this.home}>Manajemen Aset</Link>
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                            <a className="nav-link" href="/"><i className="fa fa-bell"></i></a>
                        </li>
                        <li className="nav-item dropdown">
                            <a className="nav-link dropdown-toggle" data-toggle="dropdown" href="/" role="button" aria-haspopup="true" aria-expanded="false">Dropdown</a>
                            <div className="dropdown-menu">
                                <a className="dropdown-item" href="/">Action</a>
                                <a className="dropdown-item" href="/">Another action</a>
                                <a className="dropdown-item" href="/">Something else here</a>
                                <div className="dropdown-divider"></div>
                                <Link className="dropdown-item" to="#!" onClick={this.logout}>Keluar</Link>
                            </div>
                        </li>
                    </ul>
                </nav>
            </div>
        );
    }
}

export default Topnav;