import React, { Component } from 'react';
import './Nav.css';
import { Redirect, Link } from 'react-router-dom';

class Navnya extends Component {
    constructor(props) {
        super(props);
        this.state = {
            home: false,
            habis: false,
            infra: false,
            kategori: false
        }
        this.homes = this.homes.bind(this);
        this.habis_pakai = this.habis_pakai.bind(this);
        this.infras = this.infras.bind(this);
        this.ktg = this.ktg.bind(this);
    }
    homes() {
        if (window.location.href === window.location.origin + '/') {
            window.location.reload();
        } else {
            this.setState({ home: true });
        }
    }

    habis_pakai() {
        if (window.location.href === window.location.origin + '/aset-habis-pakai') {
            window.location.reload();
        } else {
            this.setState({ habis: true });
        }
    }

    infras() {
        if (window.location.href === window.location.origin + '/infra') {
            window.location.reload();
        } else {
            this.setState({ infra: true });
        }
    }
    ktg() {
        if (window.location.href === window.location.origin + '/kategori') {
            window.location.reload();
        } else {
            this.setState({ kategori: true });
        }
    }

    render() {
        if (this.state.home) {
            return <Redirect to={"/"} />
        }
        if (this.state.habis) {
            return <Redirect to={"/aset-habis-pakai"} />
        }
        if (this.state.infra) {
            return <Redirect to={"/infra"} />
        }
        if (this.state.kategori) {
            return <Redirect to={"/kategori"} />
        }
        return (

            <div>
                <div id="sidebar-container" className="sidebar-expanded d-none d-md-block">
                    <ul className="list-group">
                        <li className="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                            <small>MAIN MENU</small>
                        </li>
                        <Link to="/" onClick={this.homes} data-toggle="collapse" aria-expanded="false" className="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                            <div className="d-flex w-100 justify-content-start align-items-center">
                                <span className="fa fa-tachometer-alt fa-fw mr-3"></span>
                                <span className="menu-collapsed">Dashboard</span>
                            </div>
                        </Link>
                        <a href="#Aset" data-toggle="collapse" aria-expanded="false" className="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                            <div className="d-flex w-100 justify-content-start align-items-center">
                                <span className="fa fa-barcode fa-fw mr-3"></span>
                                <span className="menu-collapsed">Aset</span>
                                <span className="submenu-icon ml-auto"></span>
                            </div>
                        </a>
                        <div id='Aset' className="collapse sidebar-submenu">
                            <Link to="/aset-habis-pakai" onClick={this.habis_pakai} className="list-group-item list-group-item-action bg-dark text-white">
                                <span className="menu-collapsed">Habis Pakai</span>
                            </Link>
                            <Link to="/" className="list-group-item list-group-item-action bg-dark text-white">
                                <span className="menu-collapsed">Tidak Habis Pakai</span>
                            </Link>
                        </div>
                        <Link to="/infra" onClick={this.infras} data-toggle="collapse" aria-expanded="false" className="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                            <div className="d-flex w-100 justify-content-start align-items-center">
                                <span className="fa fa-dolly-flatbed fa-fw mr-3"></span>
                                <span className="menu-collapsed">Infrastruktur</span>
                            </div>
                        </Link>
                        <a href="#submenu2" data-toggle="collapse" aria-expanded="false" className="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                            <div className="d-flex w-100 justify-content-start align-items-center">
                                <span className="fa fa-window-close fa-fw mr-3"></span>
                                <span className="menu-collapsed">Disposal</span>
                            </div>
                        </a>
                        <a href="#submenu2" data-toggle="collapse" aria-expanded="false" className="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                            <div className="d-flex w-100 justify-content-start align-items-center">
                                <span className="fa fa-broom fa-fw mr-3"></span>
                                <span className="menu-collapsed">Perawatan Aset</span>
                            </div>
                        </a>
                        <a href="#Permintaan" data-toggle="collapse" aria-expanded="false" className="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                            <div className="d-flex w-100 justify-content-start align-items-center">
                                <span className="fa fa-hand-holding fa-fw mr-3"></span>
                                <span className="menu-collapsed">Permintaan</span>
                                <span className="submenu-icon ml-auto"></span>
                            </div>
                        </a>
                        <div id='Permintaan' className="collapse sidebar-submenu">
                            <a href="/" className="list-group-item list-group-item-action bg-dark text-white">
                                <span className="menu-collapsed">Pengajuan</span>
                            </a>
                            <a href="/" className="list-group-item list-group-item-action bg-dark text-white">
                                <span className="menu-collapsed">Ketersediaan</span>
                            </a>
                        </div>
                        <a href="#Pengaturan" data-toggle="collapse" aria-expanded="false" className="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                            <div className="d-flex w-100 justify-content-start align-items-center">
                                <span className="fa fa-cogs fa-fw mr-3"></span>
                                <span className="menu-collapsed">Pengaturan</span>
                                <span className="submenu-icon ml-auto"></span>
                            </div>
                        </a>
                        <div id='Pengaturan' className="collapse sidebar-submenu">
                            <a href="/" className="list-group-item list-group-item-action bg-dark text-white">
                                <span className="menu-collapsed">Lokasi</span>
                            </a>
                            <a href="/" className="list-group-item list-group-item-action bg-dark text-white">
                                <span className="menu-collapsed">Manufaktur</span>
                            </a>
                            <a href="/" className="list-group-item list-group-item-action bg-dark text-white">
                                <span className="menu-collapsed">Pemasok</span>
                            </a>
                            <a href="/" className="list-group-item list-group-item-action bg-dark text-white">
                                <span className="menu-collapsed">Perusahaan</span>
                            </a>
                            <Link to="/kategori" onClick={this.ktg} className="list-group-item list-group-item-action bg-dark text-white">
                                <span className="menu-collapsed">Kategori</span>
                            </Link>
                            <a href="/" className="list-group-item list-group-item-action bg-dark text-white">
                                <span className="menu-collapsed">Model</span>
                            </a>
                            <a href="/" className="list-group-item list-group-item-action bg-dark text-white">
                                <span className="menu-collapsed">Status</span>
                            </a>
                        </div>
                        <a href="#Pengguna" data-toggle="collapse" aria-expanded="false" className="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                            <div className="d-flex w-100 justify-content-start align-items-center">
                                <span className="fa fa-user fa-fw mr-3"></span>
                                <span className="menu-collapsed">Pengguna</span>
                                <span className="submenu-icon ml-auto"></span>
                            </div>
                        </a>
                        <div id='Pengguna' className="collapse sidebar-submenu">
                            <a href="/" className="list-group-item list-group-item-action bg-dark text-white">
                                <span className="menu-collapsed">Daftar Pengguna</span>
                            </a>
                            <a href="/" className="list-group-item list-group-item-action bg-dark text-white">
                                <span className="menu-collapsed">Grup</span>
                            </a>
                            <a href="/" className="list-group-item list-group-item-action bg-dark text-white">
                                <span className="menu-collapsed">Tingkat</span>
                            </a>
                        </div>
                        <a href="#submenu2" data-toggle="collapse" aria-expanded="false" className="bg-dark list-group-item list-group-item-action flex-column align-items-start">
                            <div className="d-flex w-100 justify-content-start align-items-center">
                                <span className="fa fa-chart-pie fa-fw mr-3"></span>
                                <span className="menu-collapsed">Laporan</span>
                            </div>
                        </a>
                    </ul>
                </div>
            </div>
        );
    }
}

export default Navnya;