import React, { Component } from 'react';
import { PostData } from '../../services/PostData';
import logo from '../../../src/ubj.png';
import { Redirect } from 'react-router-dom';
import './Login.css';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userid: '',
            password: '',
            redirect: false
        }
        this.login = this.login.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    login() {
        if (this.state.userid && this.state.password) {
            PostData('/auth/login', this.state).then((result) => {
                let responseJson = result;
                if (responseJson.token) {
                    sessionStorage.setItem('token', responseJson.token);
                    this.setState({ redirect: true }); 
                } else {
                    alert('User ID atau Password salah...!!!');
                }
            });
        }
    }
    onChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }
    render() {
        if (this.state.redirect) {
            return <Redirect to={"/"} />
        }
        if (sessionStorage.getItem("token")) {
            return <Redirect to={"/"} />
        }
        return (
            <div>
                <div className="login-page">
                    <div className="card">
                        <div className="card-body">
                            <div className="login-box">
                                <div className="logo">
                                    <img src={logo} alt="logo" />
                                </div>
                                <div className="body">
                                    <h3 className="msg">Manajemen Aset</h3>
                                    <div className="input-group mb-2 mr-sm-2">
                                        <div className="input-group-prepend">
                                            <div className="input-group-text"><i className="fa fa-user"></i></div>
                                        </div>
                                        <input className='form-control form-control-lg' type="text" name="userid" placeholder="User ID" onKeyPress={(event) => { if (event.key === "Enter") { this.login() } }} onChange={this.onChange} autoFocus></input>
                                    </div>
                                    <div className="input-group mb-2 mr-sm-2">
                                        <div className="input-group-prepend">
                                            <div className="input-group-text"><i className="fa fa-key"></i></div>
                                        </div>
                                        <input className='form-control form-control-lg' type="password" name="password" placeholder="Password" onKeyPress={(event) => { if (event.key === "Enter") { this.login() } }} onChange={this.onChange} ></input>
                                    </div>
                                </div>
                                <div className="form-group text-right">
                                    <button className="btn btn-primary submit" type="submit" name="submit" onClick={this.login} >Masuk
                            </button>
                                </div>
                                <p className="text-center">
                                    Hubungi Dir. PTI jika terjadi kendala <br />
                                    © Dir. PTI - 2019 <br />
                                    Universitas Bhayangkara Jakarta Raya
                        </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}


export default Login;