import React, { Component } from 'react';
import Navnya from '../Menu/Navnya';
import Topnav from '../Menu/Topnav';
import '../Menu/Nav.css';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render() {
        
        return (

            <div>
                <Topnav />
                <div className="row" id="body-row">
                    <Navnya />
                    <div className="col">
                        <div className="container right_col">

                            <h2>Dashboard</h2>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Home;