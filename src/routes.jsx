import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Home from '././komponen/Home/Home';
import Login from '././komponen/Login/login';
import habis_pakai from '././komponen/Aset/Habis_pakai';
import Infrastruktur from '././komponen/Infrastruktur/Infrastruktur';
import ktg from '././komponen/Pengaturan/kategori';
import status from '././komponen/Pengaturan/Status';
import kon from '././komponen/Pengaturan/kondisi';


const Routes = () => (
  <BrowserRouter >
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/login" component={Login} />
      <Route path="/aset-habis-pakai" component={habis_pakai} />
      <Route path="/infra" component={Infrastruktur} />
      <Route path="/kategori" component={ktg} />
      <Route path="/status" component={status} />
      <Route path="/kondisi" component={kon} />
    </Switch>
  </BrowserRouter>
);

export default Routes;